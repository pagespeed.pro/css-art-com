# 🖼 CSS-ART.COM PURE CSS ART GALLERY

[CSS-ART.COM](https://css-art.com/) is a gallery for pure CSS web art and a crowdsourced test environment for [$async](https://gitlab.com/pagespeed.pro/async), a high performance CSS and script loader for frontend optimization.

The gallery is self-maintained and open source. The rankings are based on Google Analytics data.

### About CSS-ART.COM

CSS-ART.COM is visited by tens of thousands of art viewers per week from almost all countries in the world with 80% entering the website with a search related to CSS art. In 2021 the website was visited from 177 countries.

![Screenshot_2021-09-30 Analytics](international-rankings.png) ![Screenshot_2021-09-30 Analytics(1)](international-rankings-list.png) 

![Screenshot_2021-09-30 css art - Google Search](google-search-rankings.png)

### About the author

A fan of pure CSS design and once a regular follower of cssbeauty.com (down since 2006).

![CSS Beauty ](css-beauty-logo.jpg)

![CSS Beauty website 2001 ](css-beauty-page.jpg) 